# bloo's setup
**WIP**

# What I use
**Distro:** [FreeBSD](https://docs.freebsd.org/en/books/handbook/) or [Arch (Laptop)]  
**Terminal:** Alacritty  
**Shell:** fish  
**Text editor:** nvim  
**VM:** awesomewm  
**DW:** [lightdm](https://archlinux.org/packages/?name=lightdm)  
**Browser:** firefox  

## Quick Install
BSD: `gmake`
Linux: `make`

If struggling to install lightdm or any etc/ configs, `doas gmake install-etc` or `sudo make install-etc`

## Notes
* I use dvorak so some bindings maybe abit weird for qwerty users
* This assumes the installation and use of `doas` instead of `sudo`
