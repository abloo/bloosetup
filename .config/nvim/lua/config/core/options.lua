local o = vim.o
local g = vim.g

g.autoformat = false

--o.relativenumber = false

o.hlsearch = false

o.scrolloff = 8
o.colorcolumn = "80"
o.updatetime = 50
o.clipboard = ""
