.PHONY: install install-etc

OS := $(shell uname)

ifeq ($(OS), FreeBSD)
	ETC_DIR := /usr/local/etc
	SU_CMD := doas
else ifeq ($(OS), Linux)
	ETC_DIR = /etc/
	SU_CMD := sudo
endif

all: install

install:
	rm -rf ~/.config/alacritty/
	rm -rf ~/.config/awesome/
	rm -rf ~/.config/fish/
	rm -rf ~/.config/gtk-3.0/
	rm -rf ~/.config/nvim/
	rm -rf ~/.config/rofi/
	rm ~/.Xresources
	cp -r ./.config/* ~/.config/
	cp ./.Xresources ~/
	$(SU_CMD) $(MAKE) install-etc

install-etc:
	cp -r lightdm/* $(ETC_DIR)/lightdm/
